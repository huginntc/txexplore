#!/bin/bash

set -euo pipefail

protoc --gocosmos_out=plugins=interfacetype+grpc,Mgoogle/protobuf/any.proto=github.com/cosmos/cosmos-sdk/codec/types:. proto/bridge.proto
mv gitlab.com/huginntc/txexplore/bridge/bridge.pb.go bridge/bridge.pb.go
