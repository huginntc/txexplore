package main

import (
	"fmt"
	"os"
	"strings"

	"github.com/syndtr/goleveldb/leveldb"
	"github.com/syndtr/goleveldb/leveldb/opt"
	"gitlab.com/huginntc/txexplore/bridge"

	tmtypes "github.com/tendermint/tendermint/abci/types"
)

func main() {
	if len(os.Args) < 2 {
		fmt.Print("Usage: txexplore <db_path>")
		return
	}

	dbPath := os.Args[1]

	db, err := leveldb.OpenFile(dbPath, &opt.Options{ErrorIfMissing: true, ReadOnly: true})
	if err != nil {
		panic(err)
	}
	defer db.Close()

	iter := db.NewIterator(nil, nil)
	defer iter.Release()

	i := -1
	good := 0
	txins := 0
	noops := 0

	for iter.Next() {
		i++

		if i%100000 == 0 {
			key := iter.Key()
			percent := (int(key[0])*256 + int(key[1])) / 656
			fmt.Fprintf(os.Stderr, "  %d %d %d %d %d%%\r", i, good, txins, noops, percent)
		}

		if len(iter.Key()) != 32 {
			// fmt.Printf("wrong key len: i=%d keylen=%d key=%s\n", i, len(iter.Key()), iter.Key())
			continue
		}

		var txr tmtypes.TxResult
		err = txr.Unmarshal(iter.Value())
		if err != nil || txr.Tx == nil {
			// fmt.Printf("Failed to parse %d, '%s'\n", i, iter.Key())
			continue
		}

		var stx bridge.SignedTx
		err = stx.Unmarshal(txr.Tx)
		if err != nil {
			// fmt.Printf("Failed to parse2 %d, '%s'\n", i, iter.Key())
			continue
		}

		if stx.Tx == nil || stx.Tx.Tx == nil || stx.Tx.Tx.Tx == nil {
			// fmt.Printf("got nil: i=%d key=%X key=%s\n", i, iter.Key(), iter.Key())
			continue
		}

		if stx.Tx.Tx.Tag == "/types.MsgObservedTxIn" {
			txins++
			var msg bridge.MsgObservedTxIn
			msg.Unmarshal(stx.Tx.Tx.Tx)
			for _, otx := range msg.Txs {
				if strings.Contains(strings.ToLower(otx.Tx.Memo), "noop") {
					fmt.Printf("height:%d,thor_txid:%X,%+v\n", txr.Height, iter.Key(), otx.Tx)
					noops++
				}
			}
		}
		good++
	}

	fmt.Fprintf(os.Stderr, "Total records: %d, good: %d, MsgObservedTxIn: %d, noops: %d\n",
		i+1, good, txins, noops)
}
